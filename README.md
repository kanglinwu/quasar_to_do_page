# Quasar App (quasar_front_end)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### After export SPA folder - How can i run it on GitLab
> Refer to https://docs.gitlab.com/ee/user/project/pages/index.html <br>
> Refer to https://docs.gitlab.com/ee/user/project/pages/index.html
1. Create the empty folder ( Public )
2. Need provide credit card info !!!!
