// import UserPage from 'layouts/User.vue'

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: {
      keepAlive: true
    },
    children: [
      { path: '', component: () => import('src/pages/Todo.vue') },
      { path: '/help', component: () => import('src/pages/Help.vue') },
      { path: '/editor', component: () => import('src/pages/editor.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  },
  {
    path: '/attack_page',
    component: () => import('pages/attack_page.vue')
  },
  {
    path: '/user',
    component: () => import('layouts/User.vue'),
    children: [
      { path: 'post', component: () => import('pages/Posts.vue') },
      { path: 'profile', component: () => import('pages/Profile.vue') }
    ]
  }
]

export default routes
